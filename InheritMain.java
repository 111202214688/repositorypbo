import java.util.Scanner;

public class InheritMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Student mahasiswa = new Student();

        // Input nilai untuk name, address, dan nim
        System.out.print("Masukkan Nama: ");
        mahasiswa.name = scanner.nextLine();

        System.out.print("Masukkan Alamat: ");
        mahasiswa.address = scanner.nextLine();

        System.out.print("Masukkan NIM: ");
        mahasiswa.nim = scanner.nextLine();

        // Input nilai SPP, SKS, dan Modul
        System.out.print("Masukkan biaya SPP: ");
        mahasiswa.spp = scanner.nextDouble();

        System.out.print("Masukkan jumlah SKS: ");
        mahasiswa.sks = scanner.nextInt();

        System.out.print("Masukkan biaya Modul: ");
        mahasiswa.modul = scanner.nextDouble();

        // pemanggilan method dari superclass
        // pemanggilan method dari subclass
        mahasiswa.identity();
        System.out.println("Total Pembayaran: Rp " + mahasiswa.hitungPembayaran());

        // Memanggil method hobi
        mahasiswa.hobi();

        scanner.close();
    }
}

class Person {
    // atribut dan method super class
    protected String name;
    protected String address;

    public void identity() {
        System.out.println("Nama: " + name);
        System.out.println("Alamat: " + address);
    }

    // Method untuk menampilkan hobi
    public void hobi() {
        System.out.println(name + " memiliki hobi tertentu.");
    }
}

// inherit dari Person
class Student extends Person {
    String nim;
    double spp;
    int sks;
    double modul;

    // method baru di subclass
    public String getNim() {
        return nim;
    }

    // Method untuk menghitung pembayaran
    public double hitungPembayaran() {
        return spp + (sks * 150000) + modul;
    }
}
